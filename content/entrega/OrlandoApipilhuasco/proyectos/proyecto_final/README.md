# Redes de computadora
# Servidor de Correo

### **Integrantes**
```
Apipilhuasco Rosas Orlando Alain 
Salas Martínez Mauricio Javier
Fermoso Flores Omar
Colín Torres Jesús Manuel
```
### Descripción
Para el desarrollo del servidor web “mail.redes.tonejito.cf”, usamos un servidor de correo de software libre llamado Postfix, el cual es un agente de transporte  de correo electrónico, sus puntos claves son  la seguridad , la eficiencia,  y la facilidad  de configuración  y administración así como la compatibilidad con Sendmail  y otros sistemas de correo. En conjunto a Postfix agregamos el servicio Dovecot el cual es un servidor de IMAP  y POP3 de código abierto para sistemas GNU/Linux que lo más importante es que sea seguro, ligero, rapido, y facil de instalar. Como servicio de servidor web usamos Apache el cual es versátil y ligero y es completamente gratuito, cerca del 50% de las páginas web usan este servicio. Para obtener nuestro certificado SSL usamos Certbot que permite obtener certificados de Let´s Encrypt y auto configurar nuestro sistema. Para gestionar nuestros datos usamos Maria-db que es una de las bases de datos más importantes ya que es usada para manejar grandes cantidades de datos. Y finalmente para ver nuestro correo, hacemos el uso de RoundCube un cliente de correo  para ver los mensajes de nuestro mail a través de una página web desde cualquier navegador, el cual nos proporciona todo lo que necesitamos para gestionar nuestro servidor de correo.

### Instalación y configuración de Postfix
![Postfix](./Imagenes/105705420_296607968152977_1243251924810901345_n.png)
Usamos el comando 

`apt -y install postfix sasl2-bin`

para instalar este programa. Al momento de instalación nos saldrán pantallas de configuración pero elegimos No Configuration para hacerlo manualmente.
Configuramos Postfix, nuestro MTA, de acuerdo a nuestro nombre de dominio e IP. Esto lo hacemos en el archivo `/etc/postfix/main.cf` donde establecemos las siguientes variables:
```
myhostname= mail.redes.tonejito.cf
mydomain = redes.tonejito.cf
mynetworks = 127.0.0.0/8, 54.160.212.85
```
También configuramos para que Postfix trabaje con nuestro MDA, Dovecot:
```
smtpd_sasl_type = dovecot
```
La carpeta para la bandeja de entrada es Maildir:
```
home_mailbox = Maildir/
```

### Instalación y configuración Dovecot
![Dovecot](./Imagenes/75076613_294648158405516_8198415860984544120_n.png)

Instalamos con

 `apt -y install dovecot-core dovecot-pop3d dovecot-imapd`

Ahora, modificamos varios archivos para configurar Dovecot.
Al archivo /etc/dovecot/dovecot.conf descomentamos la línea 
```
listen = *, ::
```
Al archivo `/etc/dovecot/conf.d/10-auth.conf` le hacemos los siguientes cambios:
Descomentamos y cambiamos  
```
disable_plaintext_auth = no 
```
y agregamos
```
auth_mechanisms = plain login
```
Al archivo `/etc/dovecot/conf.d/10-mail.conf` modificamos 
```
mail_location = maildir:~/Maildir
```
Por último agregamos modificamos el archivo `/etc/dovecot/conf.d/10-master.conf`
```
#Postfix smtp-auth
unix_listener /var/spool/postfix/private/auth {
	mode = 0666
	user = postfix
	group = postfix
}
```
### Configuración de PHP en Apache2
![Apache2](./Imagenes/103154026_707299536775574_2526407103121615701_n.png)

Instalamos PHP con el siguiente comando: `apt -y install php php-cgi libapache2-mod-php php-common php-pear php-mbstring`.

Ahora, habilitamos la configuración de PhP con `a2enconf php7.3-cgi` y reiniciamos apache2.
Luego modificamos el archivo `/etc/php/7.3/apache2/php.ini`
```
date.timezone = “America/Mexico_City”
```

### Configuración de SSL/TLS

#### Configuración de certificados SSL
Instalamos el cliente de Cerbot: `apt -y install certbot`

![Certbot](./Imagenes/80108673_301236297725067_2718095692949093247_n.png)

Ahora, para obtener certificados, debemos tener el servidor Apache instalado y acceso desde el Internet al servidor a través del puerto 80.
Ejecutamos los siguientes comandos:
`certbot certonly --webroo -w /var/www/html -d mail.redes.tonejito.cf`.
El correo a usar es mail.redes.tonejito.cf

Cuando ya tengamos los certificados, configuramos Apache2. 
Modificamos el archivo `/etc/apache2/sites-available/default-ssl.conf`
Modificamos el email de admin:
```
ServerAdmin webmaster@localhost
```
Colocamos las rutas de los certificados ya obtenidos
```
SSLCertificateFile /etc/letsencrypt/live/mail.redes.tonejito.cf
SSLCertificateKeyFile /etc/letsencrypt/live/mail.redes.tonejito.cf
SSLCertificationChainFile /etc/letsencrypt/live/mail.redes.tonejito.cf/chain.pem
```
![LetsEncrypt](./Imagenes/106803088_278933950186196_3108554515197760762_n.png)

### Instalación y configuración de MariaDB
![MariaDB](./Imagenes/106076230_298370658206597_8932294687095743757_n.png)

Instalamos el programa con `apt -y install mariadb-server`. 
Ahora, ejecutamos `mysql_secure_installation` para configurarlo: (opciones traducidas al español)
```
Elegimos establecer una contraseña de root: y
Quitar usuarios anónimos: y
No permitir iniciar sesión remotamente: y
Quitar base de datos de prueba: y
Recargar tabla de privilegios:  y
```
#### Crear base de datos para RoundCube
![RoundCube](./Imagenes/75341211_2599335440316400_201147095943773619_n.jpg)

Entramos a la base de datos con `mysql -u root -p`
Creamos la base de datos y le damos privilegios: 
```
create database roundcube;
grant all privileges on roundcube.* to roundcube@’localhost’ identified by ‘password’;
flush privileges
```

Instalamos RoundCube: `apt -y install roundcube roundcube-mysql`
Damos que no a configurar la base de datos de roundcube con `dbconfig-common`´. 
Nos posicionamos en el directorio de instalación de roudncube: `cd /usr/share/dbconfig-common/data/roundcube/install`

Ligamos la base de datos roundcube que hemos creado antes y hacemos uso de la contraseña de roundcube que creamos a través de mariaDB: `mysql -u roundcube -D roundcube -p < mysql`

Ahora editamos el archivo `/etc/roundcube/debian-db.php`
```
$dbuser=’roundcube’;
$dbpsas=’tonejito’;
$basepath=’’;
$dbname=’roundcube’;
$dbserver=’localhost’;
$dbport=’3306’;
$dbtype=’mysql’;
```

Modificamos el archivo `/etc/roundcube/config.inc.php`
```
$config[‘default_host’] = ‘tls://mail.redes.tonejito.cf’;
$config[‘smtp_server’] = ‘tls://mail.redes.tonejito.cf’;
$config[‘smtp_port’] = 25;
$config[‘smtp_user’] = ‘%u’
$config[‘smtp_pass’] = ‘%p’
$config[‘support_url’] = ‘’;
$config[‘product name’] = ‘Tonejito RoundCube WebMail’;
$config[‘default_port’] = 143;
$config[‘smtp_auth_type’] = ‘LOGIN’;
$config[‘smtp_helo_host’] = ‘mail.redes.tonejito.cf’;
$config[‘mail_domain’] = ‘redes.tonejito.cf’;
$config[‘useragent’] = ‘Tonejito Webmail’;
$config[‘imap_conn_options’] = array(
	‘ssl’ = > array (
	‘verify_peer’ => true,
	‘verify_peer_name’ => false,
	‘CN_match’ => ‘redes.tonejito.cf’,
	‘allow_self_signed’ => true,
	‘ciphers’ => ‘HIGH:!SSLv2:!SSLv3’,
),
);
$config[‘smtp_conn_options’] = array(
	‘ssl’ => array(
	‘verify_peer’ => true,
	‘CN_match’ => ‘redes.tonejito.cf’,
	‘allow_self_signed’ => true,
	‘ciphers’ => ‘HIGH:!SSLv2:!SSLv3’,
	),
);
```

### Uso de la aplicación
Entrar a la liga https://mail.redes.tonejito.cf/roundcube/

Se tienen a dos usuarios:
```
Nombre de Usuario     
maw                       
lester			    
```

![maw_lester](./Imagenes/maw_a_lester.png)

En esta imagen el usuario maw manda un correo a lester.

![lester_respuesta](./Imagenes/lester_recibe_correo.png)

En esta pantalla se puede ver como en la bandeja de entrada del usuario lester está el correo del usuario maw.
El usuario lester le escribe una respuesta al usuario maw y le envía un correo.

![maw_respuesta](./Imagenes/maw_recibe_respuesta.png)

El usuario maw puede ver en su bandeja de entrada la respuesta que le hizo el usuario lester.
### Lista de videos (No disponible aún)
https://www.youtube.com/playlist?list=PLgUnZ9bSqo_T3-gc3nFYO85SMAS4kwx__

### Postfix configuración video
https://www.youtube.com/watch?v=AWv1LLa2VEQ&t=5s

